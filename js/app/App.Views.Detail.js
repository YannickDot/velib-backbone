App.Views.Detail = Backbone.View.extend({

    el: "#station-detail",

    template: function(model){ return template('stationDetailTmp', model.toJSON()); }, 

    initialize: function(){


        var newModel = this.fetchModel(this.options.number, this.model);
        this.model = newModel;

        //this.render(); 
        
    },

    events: {
     'click #add-favs' : 'addToFavs',
     'click #remove-favs' : 'removeFromFavs',
     'click #close-station-detail' : 'destroy_view',
     'click #itineraire' : 'getDirectionsStation'


    },

    render: function(){
        

        this.$el.html( this.template(this.model));

        var id = this.model.get('number'); 

        if ( favIndexer.indexOf(id) === -1 ) {

            displayAddFav();

        } else{

            displayRemoveFav();

        };

        this.options.notif.trigger("update");
        
        return this;  // returning this from render method..
    },

    fetchModel: function (number, currentModel) {

        var url = 'https://api.jcdecaux.com/vls/v1/stations/'+ number +'?contract=paris';

        var station, model;

        var self = this;

        //$.getJSON(url, function(data) {
            station = stationSample;
            model = new App.Models.Station(station);
            var timestamp = model.get("last_update"); 
            var updated = new Date(timestamp);
            var formattedUpdate = updated.getDate()+'-'+updated.getMonth()+'-'+updated.getFullYear()+' '+updated.getHours()+':'+updated.getMinutes()+':'+updated.getSeconds();

            model.set("updated", formattedUpdate);
            model.set('fav', currentModel.get('fav'));

            self.model = model;

            self.render();

            self.model.on("change", this.render, this);  

        //});

        return model;
    },


    destroy_view: function() {

    //COMPLETELY UNBIND THE VIEW
    this.undelegateEvents();
    this.$el.removeData().unbind(); 

    },

    addToFavs: function(){
        stationsCollection.get(this.options.number.toString()).set('fav',1);

        App.NotifID = this.options.number;
        App.NotifModel = this.model;
        this.options.notif.trigger("addFav");
        
    },

    removeFromFavs: function(){
        stationsCollection.get(this.options.number.toString()).set('fav',0);

        App.NotifID = this.options.number;
        this.options.notif.trigger("removeFav");
    },

    goToPin: function() {
        window.location.hash = 'page-home';

        var self = this;
        var carte = self.options.map;
        var pos = this.model.get('position');
        var posGoogle = new google.maps.LatLng(pos.lat, pos.lng);

        carte.map.panTo(posGoogle);

        this.destroy_view();

    }, 

    getDirectionsStation: function () {

        var pos = this.model.get('position');
        
        navigator.geolocation.getCurrentPosition(geolocationCallback);

        var start = coordinates;
        //var start = new google.maps.LatLng(48.8566140,2.3522219);
        var end = new google.maps.LatLng(pos.lat, pos.lng);

        var url = "https://maps.google.com/maps?saddr="+ start.lat() +'+'+ start.lng() +"&daddr=" + end.lat() +'+'+ end.lng() +'&dirflg=w';
        
        open_browser(url);


    }

});
