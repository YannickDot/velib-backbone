
// Station Model 

App.Models.Station = Backbone.Model.extend({
    defaults: {
        number: 0,
        name: "",
        contract: "",
        address: "",
        position: { lat: 0, lng: 0},
		banking: true,
		bonus: false,
		status: "CLOSE",
		bike_stands: 0,
		available_bike_stands: 0,
		available_bikes: 0,
		last_update: 0,
		updated: "",
		fav: 0

    },

    idAttribute: "number"
    
});


App.NotifModel = new App.Models.Station();