App.Views.SearchItem = Backbone.View.extend({
	tagName: 'li',


	template: function(){ 

        if (this.format === 'address') {
            return  template('stationSearchTmpbyAddress', this.model.toJSON()); 
        } else{ 
            return  template('stationSearchTmpbyName', this.model.toJSON()); 
        };
        
    },
    

	events: {
     'click' : 'clicked'

    },

    initialize: function(options){
        this.format = this.options.format;

        var self = this;

        this.options.notif.on("byName", function () {
            self.format = 'name';
            self.render();
        });

        this.options.notif.on("byAddress", function () {
            self.format = 'address';
            self.render();
        });


        this.model.on("change", this.render, this);
    
    },

	render: function(){

	    this.$el.html( this.template(this.model.toJSON()));
	    return this;  // returning this from render method..
	},

    clicked: function(){
        var numberStation = this.model.get("number");

        var stationDetail = new App.Views.Detail({ model : this.model, number : numberStation, notif: App.Notif, notifModel : App.NotifModel, map : this.options.map });
    }

});

