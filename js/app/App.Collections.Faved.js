App.Collections.Faved = Backbone.Collection.extend({

        model: App.Models.Station,

		localStorage: new Backbone.LocalStorage("StationsFavedStorage"),	

		url: "",

        comparator: function(station){
            return station.get("number");
        }
        
    });
