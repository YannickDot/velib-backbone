App.Views.Favs = Backbone.View.extend({
    /*tagName: 'ul',
    id: 'favs-list',
    attributes: {"data-role": 'listview'},*/

    collection: App.Collections.Faved,

    el: "#favs-list",

    initialize: function () {
        favIndexer = [];
        this.collection.each(function(station){
           favIndexer.push(station.get('number'));
        }, this);

        this.render();


        this.options.notif.on("addFav", function () {
            var num = App.NotifID;

            this.collection.add(App.NotifModel);
            favIndexer.push(num);

            this.collection.sync('create', App.NotifModel);
            
            this.render();

        }, this);

        this.options.notif.on("removeFav", function () {
            var num = App.NotifID;
            var index = favIndexer.indexOf(num);

            favIndexer.splice(index, 1);

            this.collection.sync('delete', this.collection.get(num));

            this.collection.fetch();     
            
            console.log(this.collection.toJSON());
            this.render();

        }, this);

        this.options.notif.on("update", function () {

        }, this);

    },

    render: function(){

        if (this.collection.length === 0) {
            displayFavsEmpty();
        } else{
            displayFavsList();
        };

        var self = this;
        self.favs = []; 

        this.$el.html("");
        this.collection.each(function(station){

            var stationView = new App.Views.Fav({ model: station, map : this.options.map, notif: App.Notif, notifModel : App.NotifModel });
            
            self.favs.push(stationView); 

            this.$el.append(stationView.render().el); // calling render method manually..
        }, this);

        return this; // returning this for chaining..

    },

    markFav: function(){
        this.collection.each(function(station){
            var num = station.get('number').toString();

        

        }, this);


    },

    update: function(){
        this.favs.each(function(station){
            var num = station.model.get('number');
            station.fetchModel(num);

            this.$el.append(station.render().el); // calling render method manually..
        }, this);
    }
});