App.Views.Markers = Backbone.View.extend({
 
    collection: App.Collections.Markers,

    //el:  "#tabbar-sections",


    initialize: function () {

        this.render();

        this.collection.on("reset", this.render, this);

        this.options.notif.on("removeMarkers", this.destroy , this);

    },

    

    render: function(){

        var self = this;
        self.markers = []; 

        this.collection.each(function(station){
            var stationMarker = new App.Views.Marker({ model: station, map : this.options.map, notif: App.Notif, infowindow : this.options.map.infowindow });
            
            self.markers.push(stationMarker); 
        }, this);

        return this; // returning this for chaining..

    }, 
    
    destroy: function (){

        this.markers.forEach(function(marker){
            marker.destroy();
        });

        this.markers = [];
    }
});