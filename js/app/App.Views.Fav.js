App.Views.Fav = Backbone.View.extend({
    tagName: 'li',
    className:'ui-li-static',

    template: function(){ return template('stationFavTmp', this.model.toJSON()); },

    events: {
     'click' : 'clicked'

    },

    initialize: function(){
        this.model.on("change", this.render, this);
        this.model.on("remove", this.remove, this);

        this.options.notif.on("update", function(){
            var num = this.model.get("number");
            this.fetchModel(num);
        }, this);
    },

    render: function(){
        this.$el.html( this.template(this.model.toJSON()));
        return this;  // returning this from render method..
    },

    clicked: function(){
        var numberStation = this.model.get("number");

        var stationDetail = new App.Views.Detail({ model : this.model, number : numberStation, notif: App.Notif, notifModel : App.NotifModel, map : this.options.map });

    },

    fetchModel: function (number) {
        var baseUrl = "";
        var url = baseUrl + number;
        var station, model;

        var self = this;

        //$.getJSON(url, function(data) {
            model = new App.Models.Station(stationSample);
            model.set('fav', 1);
            stationsCollection.get(number).set('fav', 1);

            self.model = model;

            self.render();

            self.model.on("change", this.render, this);  

        //});

        return model;
    }


});