App.Collections.Stations = Backbone.Collection.extend({


        model: App.Models.StationMini,
        
        localStorage: new Backbone.LocalStorage("StationsFavedStorage"),  

        initialize: function () {
            this.bind("reset", function (model, options) {
                console.log("Inside event");
                console.log(model); 
            });
        },

        comparator: function(station){
            return station.get("number");
        },  

        parse: function(resp) {
          return resp.contract_json;
        }   

    });






