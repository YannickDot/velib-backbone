App.Views.SearchList = Backbone.View.extend({
 
    collection: App.Collections.Stations,

    el: "#search-list",

    initialize: function () {
        
        var self = this;

        this.collection.on("reset", function (){
                this.render();
                
            }, this);
        
        this.format = 'name';
        this.render();
    },

    render: function(){
        this.$el.html("");
        this.collection.each(function(station){
            var stationView = new App.Views.SearchItem({ model: station , map : this.options.map,  notif: App.Notif, notifModel : App.NotifModel, format : this.format });
            //var stationMarker = new App.Views.Marker({ model: station, map : this.options.map, notif: App.Notif, infowindow : this.options.map.infowindow });
            this.$el.append(stationView.render().el); // calling render method manually..
        }, this);

        return this; // returning this for chaining..

    }
});