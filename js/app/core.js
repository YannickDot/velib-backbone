
searchInit = false;

// cacher le splashscren et récupérer la position actuelle 
function splashscreen () {
	splashCount = true;
	$('#page-splash').fadeOut();

	document.removeEventListener('touchmove', preventDefault, false);
	document.webkitCancelFullScreen();
	App.Notif.trigger('update');
	navigator.geolocation.getCurrentPosition(geolocationCallback);
}


function preventDefault(e) { e.preventDefault(); };
document.addEventListener('touchmove', preventDefault, false);


$( document ).bind( "pageshow", function( event, data ){
	resizeMap();
});

function resizeMap(){
	google.maps.event.trigger(map, 'resize');
}


// empecher le scroll intempestif sur la map google 

$(document).ready(function(){

	document.getElementById('page-home').addEventListener('touchmove', function(e) {
	  // This prevents native scrolling from happening.
	  e.preventDefault();
	}, false);

});







// events 

function displayRemoveFav() {
	$('#add-favs').removeClass('shown').addClass('hidden');
    $('#remove-favs').removeClass('hidden').addClass('shown');
}


function displayAddFav() {
	$('#remove-favs').removeClass('shown').addClass('hidden');
    $('#add-favs').removeClass('hidden').addClass('shown');
}



function displayFavsEmpty() {
	$('#favs-list').removeClass('shown').addClass('hidden');
    $('#favs-empty').removeClass('hidden').addClass('shown');
}


function displayFavsList() {
	$('#favs-empty').removeClass('shown').addClass('hidden');
    $('#favs-list').removeClass('hidden').addClass('shown');
}



function closeTrick(id) {
	$('#'+id).click();
}



// déclenche les évenements sur la vue "recherche" pour changer son format d'affichage (par nom ou par adresse)
function displayByAddress() {
	App.Notif.trigger("byAddress");
}

function displayByName() {	
	App.Notif.trigger("byName");
}


$( document ).ready(function() {
	$('#search-by-name').click(displayByName);
	$('#search-by-address').click(displayByAddress);
});



															/* GEOLOCATION */ 


function getPosition () {

	markerActualPosition = new google.maps.Marker({
                  position: coordinates,
                  map: map.map,
                  title:"You are here!"
              });

	map.map.panTo(markerActualPosition.position);
}

function geolocationCallback (position) {

	var infopos = "Position déterminée :\n";
	infopos += "Latitude : "+position.coords.latitude +"\n";
	infopos += "Longitude: "+position.coords.longitude+"\n";
	infopos += "Altitude : "+position.coords.altitude +"\n";
	console.log(infopos);

	coordinates = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

	return coordinates;               
}


function reverseGeocoding (marker) {
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {

          console.log(results[1].formatted_address);
         
        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
}






// opens Tizen web browser to URL 

function open_browser(urlWeb){
	var appControl = new tizen.ApplicationControl("http://tizen.org/appcontrol/operation/view", urlWeb, null, null, null);

	tizen.application.launchAppControl(appControl, null, function() {
		console.log("launch application control succeed"); 
	},
	function(e) {
		console.log("launch application control failed. reason:" + e.message); 
	}, null);
}





												/* TRANSLATION */



function setToFrench () {

	$("#btn-prendre .section-txt").html('<img class="section-icon" src="assets/take_byke.png"> PRENDRE');
	$("#btn-deposer .section-txt").html('<img class="section-icon" src="assets/take_byke.png"> DEPOSER');


	$("#my-pos a").html('Ma position');

	$(".no-favs").html('PAS DE FAVORIS <br> POUR LE MOMENT');

	$("#ticket1 > p:nth-child(2)").html('Achat Ticket');
	$("#ticket7 > p:nth-child(2)").html('Achat Ticket');

	$("#ticket1 > p:nth-child(3)").html('1 JOUR');
	$("#ticket7 > p:nth-child(3)").html('7 JOURS');

	$(".actualite-text > p:nth-child(1)").html("INFOS");
	$(".sub-actu").html('L\'actualité Velib !');

	$(".follow p.heading").html('SUIVEZ NOUS');

	$('.lang > ul:nth-child(1) > li:nth-child(1) > p:nth-child(1)').html('LANGUES');


}


function setToEnglish () {

	$("#btn-prendre > p:nth-child(1)").html('<img class="section-icon" src="assets/take_byke.png"> RELEASE');
	$("#btn-deposer > p:nth-child(1)").html('<img class="section-icon" src="assets/take_byke.png"> RETURN');


	$("#my-pos a").html('My position');

	$(".no-favs").html('NO FAVORITES FOR <br> THE MOMENT');

	$("#ticket1 > p:nth-child(2)").html('Buy Ticket');
	$("#ticket7 > p:nth-child(2)").html('Buy Ticket');

	$("#ticket1 > p:nth-child(3)").html('1 DAY');
	$("#ticket7 > p:nth-child(3)").html('7 DAYS');

	$(".actualite-text > p:nth-child(1)").html("INFOS");
	$(".sub-actu").html('Velib news !');

	$(".follow p.heading").html('FOLLOW US');

	$('.lang > ul:nth-child(1) > li:nth-child(1) > p:nth-child(1)').html('LANGUAGES');

	$('input:radio[name=radio-choice-1]').filter('[value=2]').prop('checked', true);

}


