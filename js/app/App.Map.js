App.Map = Backbone.View.extend({
	
	el: "#map",

	initialize: function(){
      	var center = new google.maps.LatLng(48.85661, 2.35222);

        var styles = [ { "featureType": "poi", "elementType": "geometry.stroke", "stylers": [ { "visibility": "off" } ] },{ "featureType": "poi.sports_complex", "stylers": [ { "visibility": "off" } ] },{ "featureType": "poi.attraction", "stylers": [ { "visibility": "off" } ] },{ "featureType": "poi.medical", "stylers": [ { "visibility": "off" } ] },{ "featureType": "poi.place_of_worship", "stylers": [ { "visibility": "off" } ] },{ "featureType": "poi", "stylers": [ { "visibility": "on" } ] } ];

      	var mapOptions = {
            center: center,
            zoom: 12,
            style: styles,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl:false,
            streetViewControl:false,
            mapTypeControl:false,
            panControl:false,
            overviewMapControl:false,
            rotateControl:false
           
        };

      	this.map = new google.maps.Map(document.getElementById('map'), mapOptions);

        this.map.infowindow = new google.maps.InfoWindow({
            content : '',
            maxWidth: 200
          });

        this.resize();



    },

    resize: function() {
        var h = $(window).height() - $('#header-home').height() - $('#navbar-sections').height();
        var dH = h + 'px';
        $('#map').css({height:dH});
    }

});


