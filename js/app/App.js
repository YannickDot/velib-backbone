

window.App = {   
    Models: {},
    Collections: {},
    Views: {},
    Router: {},
    NotifModel : {},
    Notif : new _.extend({}, Backbone.Events),
    NotifID : 0,
    Map: {},

    // récupère le json d'un arrondissement pour afficher les pins sur la map
    fetchSecteur: function (value) {
        var data;

        if (value > 20) {
            data = secteur[21];

        } else{
            data = secteur[value];

        };

        markersCollection.reset(data);
               
    },

    // récupère le json d'une station grace a son champ "number" pour l'afficher son détail
    fetchStation: function (value) {
        var baseUrl = 'https://api.jcdecaux.com/vls/v1/stations/'+ value +'?contract=paris';

        var station;

        $.getJSON(baseUrl, function(data) {
            station = data;
        });

        return new App.Models.Station(station);
    },


    init : function () {

        $.mobile.page.prototype.options.domCache = true; // set page cache

        map = new App.Map();

        stationsJSON = [];
        stationsCollection = {};
        searchList = {};

        stationsCollection = new App.Collections.Stations(stationsJSON);
        searchList = new App.Views.SearchList({ collection: stationsCollection, notif: App.Notif, notifModel : App.NotifModel, map : map  });

        markersCollection =  new App.Collections.Markers();
        markerlist = new App.Views.Markers({ collection: markersCollection, notif: App.Notif, notifModel : App.NotifModel, map : map  });

        favCollection = new App.Collections.Faved();
        favCollection.fetch();
        favsList = new App.Views.Favs({ collection: favCollection, notif: App.Notif, notifModel : App.NotifModel, map : map  });

        stationsCollection.reset(listeStations);

        App.fetchSecteur("1");
    },
 

    
};


$( document ).ready(function() {
    App.init();

    $("#secteur").change(function(){
        var value = $(this).find(":selected").val();

        App.Notif.trigger("removeMarkers");

        App.fetchSecteur(value);
        map.map.setZoom(13);
        map.map.setCenter(new google.maps.LatLng(48.8566140, 2.3522219));

    });

});

window.template = function (id, options) {
    //console.log(id);
    return _.template( $('#'+id).html(), options);
};

// When ready...
    window.addEventListener("load",function() {
        // Set a timeout...
        setTimeout(function(){
            // Hide the address bar!
            window.scrollTo(0, 1);
            console.log('hidebar')
        }, 0);
    });

