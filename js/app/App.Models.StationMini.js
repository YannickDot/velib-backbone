
// Station Model 

App.Models.StationMini = Backbone.Model.extend({
    defaults: {
        number: 0,
        name: "",
        contract: "",
        address: "",
        position: { lat: 0, lng: 0},
		banking: true,
		bonus: false,
		status: "CLOSE",
		bike_stands: 0,
		available_bike_stands: 0,
		available_bikes: 0,
		last_update: 0,
		fav: 0,
		updated: ""

    },

    idAttribute: "number"
    
});


App.NotifModel = new App.Models.Station();