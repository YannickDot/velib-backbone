App.Views.Marker = Backbone.View.extend({

	 el:  "#navbar-sections",

    initialize: function(options) {
  
      var self = this;
      var carte = self.options.map;

      var pos = this.model.get('position');

      this.marker = new google.maps.Marker({
          map: carte.map,
          position: new google.maps.LatLng(pos.lat, pos.lng),
          icon : this.setPin('bikes'),
          title: this.model.get('name'),
          descr : this.model.get('address'),
          id : this.model.get('number')
      });

      this.lastpopup

      this.listener = google.maps.event.addListener(this.marker, 'click', this.show_popup);


      this.options.notif.on("deposerToggle", function () {
            this.marker.setIcon(self.setPin('stands'));
        }, this);

      this.options.notif.on("prendreToggle", function () {
            this.marker.setIcon(self.setPin('bikes'));
        }, this);

      this.options.notif.on("view"+this.marker.id, function(){
        self.showDetails();
      }, this);
      

      //this.options.notif.on("removeMarkers", this.destroy , this);

    },

    events: {
     'click #btn-deposer' : 'deposerActive',
     'click #btn-prendre' : 'prendreActive'

    },

    destroy : function(){

      $('#'+this.id).unbind();
      google.maps.event.removeListener(this.listenerHandle);
      this.marker.setMap(null);
      this.el = {};
      this.events = {};
      this.marker = {};



      this.undelegateEvents();
      /*
      this.unbind();
      this.$el.removeData().unbind(); */

    },

    render: function() { },

    show_popup : function(){
      var self = this;
      var id = self.id;

      map.map.infowindow.setContent('<div id='+ this.id +'>'+ this.title.substring(8,this.title.length) +' <br>'+ this.descr +'</div>');

      this.map.infowindow.open(this.map, this);

      var idSelector = '#'+id;

      $(idSelector).on('click', function(){
        App.Notif.trigger('view'+id);
      });


    },

    hide_popup : function(){
      infowindow.close();
    },

    deposerActive : function(){
      var self = this;
      this.marker.setIcon(self.setPin('stands'));
      
    },

    prendreActive : function(){
      var self = this;
      this.marker.setIcon(self.setPin('bikes'));
      
    },

    showDetails: function(){
        var numberStation = this.model.get("number");

        var stationDetail = new App.Views.Detail({ model : this.model, number : numberStation, notif: App.Notif, notifModel : App.NotifModel, map : this.options.map });

        window.location.hash = 'page-detail';
    },

    setPin: function (option) {
      var name = this.model.get("name");
      var status = this.model.get("status");
      var stands = this.model.get("available_bike_stands");
      var bikes = this.model.get("available_bikes");
      var bonus = this.model.get("bonus");
      var type;

      if (option === 'bikes') {
        type = bikes;
      } else{
        type = stands;
      };


      if (bonus === true) {

        if (status === "CLOSE") {

          return "assets/pin/pin_close_bonus.png"

        } else{

          if (type > 20) {
            
            return "assets/pin/pin_open_bonus_20_plus.png"

          } else{

            return "assets/pin/pin_open_bonus_"+ type +".png"

          }

        }

      } else{

          if (status === "CLOSE") {

            return "assets/pin/pin_close.png"

          } else{

            if (type > 20) {

              return "assets/pin/pin_open_20_plus.png"

            } else{

              return "assets/pin/pin_open_"+ type +".png"

            }

          }

        }
    }
})
